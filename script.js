const createTowersAndDiscs = () => {
    const caixaTorre1 = document.querySelector('#caixaTorre1');

    const torreStart = document.createElement('div');
    torreStart.classList.add('torre', 'torre-start');
    caixaTorre1.insertBefore(torreStart, document.querySelector('#startTitle'));

    const bloco1 = document.createElement('div');
    bloco1.classList.add('bloco', 'bloco1');
    torreStart.appendChild(bloco1);

    const bloco2 = document.createElement('div');
    bloco2.classList.add('bloco', 'bloco2');
    torreStart.appendChild(bloco2);

    const bloco3 = document.createElement('div');
    bloco3.classList.add('bloco', 'bloco3');
    torreStart.appendChild(bloco3);

    const bloco4 = document.createElement('div');
    bloco4.classList.add('bloco', 'bloco4');
    torreStart.appendChild(bloco4);



    const caixaTorre2 = document.querySelector('#caixaTorre2');

    const torreOffset = document.createElement('div');
    torreOffset.classList.add('torre', 'torre-offset');
    caixaTorre2.insertBefore(torreOffset, document.querySelector('#offsetTitle'));



    const caixaTorre3 = document.querySelector('#caixaTorre3');

    const torreEnd = document.createElement('div');
    torreEnd.classList.add('torre', 'torre-end');
    caixaTorre3.insertBefore(torreEnd, document.querySelector('#endTitle'));
}

createTowersAndDiscs();

const content = document.querySelector('.content');

const paragraph = document.createElement('p');
paragraph.classList.add('paragraph');
content.insertBefore(paragraph, document.querySelector('#caixaTorre3').nextSibling);

const torres = document.querySelectorAll(".torre");

for (let i = 0; i < torres.length; i++) {
    torres[i].addEventListener("click", identificarTorre);
}

let pegouDisco = ""

function identificarTorre(e) {
    const torreClicada = e.currentTarget;

    let numDiscos = torreClicada.childElementCount;

    const disco = torreClicada.lastElementChild;

    if (pegouDisco === "" && numDiscos !== 0) {
        pegouDisco = disco;
    }
    else if (pegouDisco !== '' && numDiscos === 0) {
        torreClicada.appendChild(pegouDisco);
        pegouDisco = ""
    }
    else if (pegouDisco !== '' && pegouDisco.clientWidth < disco.clientWidth) {
        torreClicada.appendChild(pegouDisco);
        pegouDisco = '';
    }
    else if (pegouDisco !== '' && pegouDisco.clientWidth > disco.clientWidth) {
        pegouDisco = '';
    }

    statusMao(e);
    return vitoria(e)

}

function vitoria(e) {
    if (e.currentTarget.childElementCount === 4 && e.currentTarget === torres[2]) {
        const espaçoFrase = document.querySelector(".content");
        const frase = document.createElement("div");
        frase.classList.add("frase-estilo");
        frase.innerText = "Parabéns! Você conseguiu!";
        espaçoFrase.appendChild(frase);
    }
}

function statusMao(e) {
    if (pegouDisco === '') {
        paragraph.innerText = 'Você soltou um disco';
    } else {
        paragraph.innerText = 'Você pegou um disco';
    }
}

